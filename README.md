# DiskMaster2

**Description**:  DiskMaster2 is simply one of the very best directory utilities ever, even on a system not lacking good tools of this kind as the Amiga.
It is something that is not very common among nowadays applications:

    it is fast even on slower processors.
    it is short and very memory-resource friendly.
    it works from an otherwise empty disk and kick 3.1.
    it is easy to use.
    it is easy to configurate in a hardly limited way.
    It comes for free.

  - **Technology stack**: to-do
  - **Status**:  [CHANGELOG](https://gitlab.com/amigasourcecodepreservation/diskmaster2/blob/master/CHANGELOG.md).

**Screenshot**:

![TO-DO](TO-DO)


## Dependencies

* TO-DO...

## Installation

* TO-DO... 


## Configuration

TO-DO

## Usage

TO-DO

## How to test the software

TO-DO

## Known issues

TO-DO

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

Contact your old amiga friends and tell them about our project, and ask them to dig out their source code or floppies and send them to us for preservation.

Clean up our hosted archives, and make the source code buildable with standard compilers like devpac, asmone, gcc 2.9x/Beppo 6.x, sas/c ,vbcc and friends.


Cheers!

Twitter
https://twitter.com/AmigaSourcePres

Gitlab
https://gitlab.com/amigasourcecodepreservation

WWW
https://amigasourcepres.gitlab.io/

     _____ ___   _   __  __     _   __  __ ___ ___   _   
    |_   _| __| /_\ |  \/  |   /_\ |  \/  |_ _/ __| /_\  
      | | | _| / _ \| |\/| |  / _ \| |\/| || | (_ |/ _ \ 
     _|_| |___/_/ \_\_|_ |_|_/_/_\_\_|__|_|___\___/_/_\_\
    / __|/ _ \| | | | _ \/ __| __|  / __/ _ \|   \| __|  
    \__ \ (_) | |_| |   / (__| _|  | (_| (_) | |) | _|   
    |___/\___/_\___/|_|_\\___|___|__\___\___/|___/|___|_ 
    | _ \ _ \ __/ __| __| _ \ \ / /_\_   _|_ _/ _ \| \| |
    |  _/   / _|\__ \ _||   /\ V / _ \| |  | | (_) | .` |
    |_| |_|_\___|___/___|_|_\ \_/_/ \_\_| |___\___/|_|\_|

----

## License

Distributed under the terms of the MIT License. See the [LICENSE](https://gitlab.com/amigasourcecodepreservation/diskmaster2/LICENSE) file for details.

----

## Credits and references

DiskMaster2 was written and developed by Greg Cunningham, Rudolph Riedel took over the development after receiving the source officially from Greg in march 1997. Jody Tierney contributed to the project. The source was released under MIT License 2018.

